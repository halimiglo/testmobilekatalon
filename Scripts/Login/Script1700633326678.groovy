import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//Mobile.startApplication('C:\\Users\\0815\\Downloads\\base.apk', true)
Mobile.startApplication('C:\\Users\\0815\\Downloads\\app-debug (2).apk', true)

Mobile.tap(findTestObject('Login/field_UsernameNew'), 0)

//Mobile.clearText(findTestObject('Login/field_UsernameEdit'), 0)
Mobile.setText(findTestObject('Login/field_UsernameNewEdit'), 'receptionist@kaf.co.id', 0)

Mobile.pressBack()

Mobile.tap(findTestObject('Login/field_PasswordNew'), 0)

Mobile.setText(findTestObject('Login/field_PasswordNewEdit'), 'R3cept!#888', 0)

Mobile.pressBack()

Mobile.tap(findTestObject('Login/btnLogin'), 0)

