import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords

MobileBuiltInKeywords mobileBuiltInKeywords = new MobileBuiltInKeywords()

Mobile.tap(findTestObject('Object Repository/Add Detail 2/button_CustomerUpdate'), 0)

Mobile.tap(findTestObject('Add Detail 2/list_CustomerUpdateRow1New'), 0)

Mobile.tap(findTestObject('Object Repository/Add Detail 2/android.widget.ImageView (3)'), 0)

Mobile.tap(findTestObject('Object Repository/Add Detail 2/android.view.View (1)'), 0)

Mobile.tap(findTestObject('Add Detail 2/button_AddLeadsUpdate'), 0)

Mobile.tap(findTestObject('Object Repository/Add Detail 2/android.view.View (2)'), 0)

Mobile.tap(findTestObject('Object Repository/Add Detail 2/android.widget.Button'), 0)

Mobile.tap(findTestObject('Object Repository/Add Detail 2/android.view.View (3)'), 0)

Mobile.tap(findTestObject('Object Repository/Add Detail 2/android.widget.Button (1)'), 0)

Mobile.tap(findTestObject('Object Repository/Add Detail 2/android.widget.EditText'), 0)

//Mobile.tapAtPosition(500, 1000)

Mobile.tap(findTestObject('Add Detail 2/list_AttendessRow1'), 0)

Mobile.tap(findTestObject('Object Repository/Add Detail 2/android.widget.Switch'), 0)

Mobile.tap(findTestObject('Object Repository/Add Detail 2/android.widget.EditText (1)'), 0)

Mobile.tap(findTestObject('Object Repository/Add Detail 2/android.view.View (7)'), 0)

Mobile.tap(findTestObject('Object Repository/Add Detail 2/android.widget.ImageView (4)'), 0)

Mobile.tap(findTestObject('Object Repository/Add Detail 2/android.view.View (8)'), 0)

Mobile.tap(findTestObject('Object Repository/Add Detail 2/android.widget.EditText (2)'), 0)

Mobile.setText(findTestObject('Object Repository/Add Detail 2/android.widget.EditText (3)'), 'Bogor', 0)

Mobile.pressBack()

Mobile.tap(findTestObject('Object Repository/Add Detail 2/android.widget.Button (2)'), 0)

Mobile.tap(findTestObject('Add Detail 2/field_CustomerPICName'), 0)

Mobile.setText(findTestObject('Add Detail 2/field_CustomerPICNameEdit'), 'Budi', 0)

Mobile.tap(findTestObject('Add Detail 2/field_CustomerPICPosition'), 0)

Mobile.setText(findTestObject('Add Detail 2/field_CustomerPICPositionEdit'), 'Manager', 0)

Mobile.tap(findTestObject('Object Repository/Add Detail 2/android.widget.Button (3)'), 0)

//Mobile.scrollToText('Cancel', FailureHandling.STOP_ON_FAILURE)

//MobileBuiltInKeywords.scrollToBottom()
Mobile.tap(findTestObject('Object Repository/Add Detail 2/android.widget.Button (4)'), 0)

Mobile.tap(findTestObject('Object Repository/Add Detail 2/android.widget.ImageView (5)'), 0)

Mobile.tap(findTestObject('Object Repository/Add Detail 2/android.widget.ImageView (6)'), 0)

Mobile.tap(findTestObject('Object Repository/Add Detail 2/android.widget.Button (5)'), 0)

Mobile.pressBack()

Mobile.closeApplication()

